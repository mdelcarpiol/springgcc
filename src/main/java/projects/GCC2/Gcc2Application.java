package projects.GCC2;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Gcc2Application {

	public static void main(String[] args) {
		SpringApplication.run(Gcc2Application.class, args);
	}

}
